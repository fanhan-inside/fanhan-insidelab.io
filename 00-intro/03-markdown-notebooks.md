---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# 掺和了可执行内容的文档

简单说，Jupyter Book允许干这事，在样式充沛的文档当中掺和可执行内容。

## 示例

首先在文档当中定义个代码块：

```{code-cell}
print(2 + 2)
```

当编译的时候，`{code-cell}`代码块当中的内容会被当前Jupyter环境的Kernel执行，而输出则显示在文档当中代码块之后。

## 创建掺和了可执行内容的文档

两件事：

1. 在文档开头添加YAML元数据，指示如何转换文本，以及执行代码所用的Kernel。
2. 在使用`{code-cell}`指令的代码块当中的内容会按照指示执行。

## 在文档中快速添加YAML元数据

使用如下命令添加当前Jupyter Book环境的元数据到指定的MyST文档当中：

```{code-block} shell
jupyter-book myst init path/to/markdownfile.md
```
