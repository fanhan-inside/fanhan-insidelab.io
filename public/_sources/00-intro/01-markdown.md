# 普通文档

其实并不普通，因为Jupyter使用Sphinx引擎编译的文档，源代码输入除了Jupyter Notebooks (`.ipynb`)之外，就是
比较流行的带格式纯文本文档之主要变体**MyST Markdown** (`.md`)。

## 啥是「MyST」？

就是「Markedly Structured Text」的缩写，有别于"CommonMark" markdown，增加了一些语法扩展以允许使用**roles**和**directives**指导Sphinx ecosystem编译。

详见官方网站说明。

## 关于Roles和Directives的例子

简单说，行内指令叫「Role」，块指令叫「Directive」。

### 特别提示代码块

比方说，在默认模板当中，对于「代码块」这种Markdown内置的功能，可以运用「Directive」扩展为以下这些样式：

```{seealso}
Here is a seealso
```

```{attention}
Here is a attention
```

```{caution}
Here is a caution
```

```{danger}
Here is a danger
```

```{error}
Here is a error
```

```{hint}
Here is a hint
```

```{important}
Here is a important
```

```{note}
Here is a note
```

```{tip}
Here is a tip
```

```{warning}
Here is a warning
```

````{versionadded} 3.1415926
```{note}
注：此乃TeX版本号
```
````

````{versionchanged} 2.71818
```{note}
注：此乃Picasso版本号
```
````

当编译文档时，都会渲染为特别的区域，如同实体出版物和一些充沛着技术文档的站点（包括但不限于W3C）的常见样式。

当然也可以换一种语法（MyST简直就是大杂烩，搜罗了大批纯文本下指定样式的各种伎俩，应收尽收，应检尽检），使用`{admonition}`并指定`:class:`的方式实现，比如：

:::{admonition} This *is* also **Markdown**
:class: warning

This text is **standard** _Markdown_
:::

### 数学公式代码块

费马定理：

$$
a^n + b^n = c^n \quad (n > 2, \enspace n \in \mathbb{Z})
$$

欧拉公式：

:::{math}
e^{i\pi}+1=0
:::

质能转换：

```{math}
e=mc^2
```

### 行内样式

而这里则是行内「role」样式，如内嵌公式{math}`a^2 + b^2 = c^2`，又如文档：{doc}`markdown-notebooks`。

## 其它内容

还有些样式需要安装扩展（包括但不限于`<dl>`列表，以及amsmath/amslatex扩展公式显示）并单独配置（率先生成conf.py之后再手改），目前为了保持默认模板的现状，并且也没有迫在眉睫的需求，暂时不折腾。

```{admonition} 特别需要注意的是
:class: caution
因为目前编译环境需要Jupyter Book本身版本0.13，而依赖的Sphinx版本则是4.5.0，曾经一时手欠升级之后发现用不了，还得降级<s>安排，控制使用，就地消化，逐步淘汰</s>之后才能顺利贯彻落实方针政策路线计划并完成。
```
