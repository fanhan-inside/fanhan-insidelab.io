# Welcome to «_Das Buch Der Kargsburg_»

这是由Jupyter Book自动生成的「書」，只能运用Markdown等有限格式，无论内容与形式都与手写HTML的页面无法相比。

本站作为第三个「个人博客站点」，主要试验技术文档类型的内容，包括但不限于公式、代码与绘图。

## 其它相关站点

+ **希帕索斯永垂不朽**：[https://fanhan-inside.bitbucket.io](https://fanhan-inside.bitbucket.io)
  + 主要的个人博客站点，积累了迄今为止公开发表的各种文章，建立站点之前的内容也尽量搜集。
+ **囧斋**：[https://fanhan-outside.vercel.app](https://fanhan-outside.vercel.app)
  + 试验性个人博客站点，目的是增加功能精简样式，顺便整理原博客站点当中的内容索引及其它思路和脑洞。

## 本站建设宗旨

1. 因为在目前运用预定义模板的情况下无法手写样式以及嵌入自定义内容，包括但不限于自制字体，所以尽量试验在正文中直接使用多语种内容观察显示效果。
2. 因为默认模板就是比较简洁美观的分栏形式，页面两侧都有导航栏，所以尽量将章节的组织架构按照实体出版物以及互联网上数字文档惯例排列方式整理。
3. 因为Jupyter本身支持多种Kernel（包括但不限于SageMath、GNU Octave），但是不知道是否更换后端之后的包含输出的文档还能被顺利编译，所以目前尽量使用默认的Python写一些涉及计算与绘图的代码并显示。